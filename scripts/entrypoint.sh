#!/bin/sh

set -e

python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

#Run uWSGI as a TCP socket on port 9000 with 4 workers (docker containers) as the master service on terminal: passing the app wsgi (./app/wsgi.py)
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi

